import os
import re

from nltk.tokenize import sent_tokenize

import _constant_paths as paths
import _constant_abbreviations as abbrs

def clean_line(string):
    """
    This function removes in the given string :
    - tabs, commas, semicolons, quotation marks (avoiding problems with CSV file format)
    - URLs, e-mail addresses, numeric data, special characters
    Furthermore, it cuts the string into sentences.
    :param string: the given character string
    :return: array: all the (cleaned) sentences present in the given string.
    """
    # Removing potential tabs
    string = re.sub(r'\t', ' ', string)
    # Removing URLs, e-mail addresses and numeric data
    string = re.sub(r'https?://[^ ]+', '', string)  # URLs
    string = re.sub(r'[a-zA-Z._-]+@[a-zA-Z._-]+\.\w{2,4}', '', string)  # e-mail addresses
    string = re.sub(r'\b\d([^ \n]*\d)?(\b|g)', '', string)  # g for grams, frequent in cooking recipes
    string = re.sub(r'[\x80-\xA0]', '', string)  # Special chars
    # Removing commas, semicolons and quotation marks
    string = re.sub(r'[,;\'"]', '', string)
    # Sentence segmentation with NLTK
    sentences = sent_tokenize(string)
    return sentences

with open(paths.OUTPUT, 'w', encoding='utf-8') as output_f:
    print("Fichier de sortie (ré)initialisé.")

with os.scandir(paths.INPUT) as dir_iterator:
    for family_dir in dir_iterator:
        # Value for the CSV's 1st column = dialect family
        dialect_family = abbrs.dialect_families_abbr[family_dir.name]
        print('Famille de dialecte en cours de traitement : {}'.format(dialect_family))
        with os.scandir(family_dir.path) as dirs_of_family:
            for dialect_dir in dirs_of_family:
                # Value for the CSV's 2nd column = dialect
                dialect_abbr = abbrs.dialects_abbr[dialect_dir.name]
                print('Dialecte en cours de traitement : {}'.format(dialect_abbr))
                with os.scandir(dialect_dir.path) as dialect_corpus:
                    # Getting all file paths in the folder and its subfolders
                    dialect_corpus_files = []
                    for entry in dialect_corpus:
                        if entry.is_dir():
                            with os.scandir(entry.path) as subdir_iterator:
                                for subentry in subdir_iterator:
                                    if subentry.is_file():
                                        dialect_corpus_files.append(subentry.path)
                        if entry.is_file():
                            dialect_corpus_files.append(entry.path)
                    nb_files = len(dialect_corpus_files)
                    print("Nombre de fichiers pour le dialecte \'{}\' : {}".format(dialect_abbr, nb_files))
                    for corpus_file_path in dialect_corpus_files:
                        with open(corpus_file_path, 'r', encoding='utf-8') as original_f:
                            file_sentences = []
                            for line in original_f:
                                line_sentences = clean_line(line)
                                for cell in line_sentences:
                                    # Getting non-empty sentences
                                    if re.search(r'\w', cell):
                                        file_sentences.append(cell)
                        # Adding cleaned and non-empty sentences to the CSV file
                        with open(paths.OUTPUT, 'a', encoding='utf-8') as output_f:
                            for sentence in file_sentences:
                                output_f.write('{}\t{}\t{}\n'.format(dialect_family, dialect_abbr, sentence))


