import os
import datetime

ROOT = os.path.dirname(__file__)

INPUT = ROOT + '/input_corpus/'
OUTPUT = ROOT + '/german_dialects{}.csv'.format(datetime.date.today())
