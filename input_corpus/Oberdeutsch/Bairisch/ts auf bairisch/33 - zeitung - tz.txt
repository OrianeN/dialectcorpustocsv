Oide Tradition zur Fastnzeit

Starkbierzeid: Mitm Salvator is oiss oganga
Da Zuber Martin (52) woaß oiss iwas Stoarkbier – er is aa Braumoasdda bei Paulana (om). Am Nockherberg bringa de Käinarinna wieda vui Massn zu de Leit.

Minga - Starkbierzeid is wieder in Manga und Bayern – do stähd a lange Tradition dahindda.

As Bierbraun am Nockherberg, des hod ­ogfangt, wia de Mönche um 1621 ins Klosta Neideck ob der Au eizong san. Da Gründa, da von Paola Franz, hot as Gelübde zur lemslanga strenga Fastnzeit obgem. Des hod ghoassn, dass de Briada newa Öl, Kreitelwerk, am gsäichdn Fleisch und Wein aa Bier hom dringa deaffa. Weil des flüssige Brod, wias des starke und gsunde Bier damois ghoassn ham, des war de wichddigste Nahrung. Scho boid war da Paulana-Mönch oana vo de beliebtestn in Minga. Eh klar, wei de Stodara und de Bauern woiddn des süffige Bier dringa. Awa de andan Brauarein hod des gor ned basst. Am 24. Februar san de Braua zum Buaga­moasdda vo Minga ganga und ham se bschwert. Vabietn soidd er as Vakaffa vom Bier am Klosta, wei des bloß fia dahoam sei soidd, oiso privat. Des hod awa nix brochd. Doch somit is de Paulana-Brauarei zum ersddn Moi in de Annaln eiganga – deshoib guidd da 24. Februar 1634 ois Gründungsdog.

Da Paola Franz hod imma am 2. April, des war da Fesddog vom Ordnsgründa, inoffiziell des Bier ausgschenkdd. Wia as Kloster gschlossn worn is, hod da Zacherl Franz Xaver de Tradition weidagfüahd. Awa er hods, wia ses in da Fastnzeid ghead, des Starkbierfestl zwischn Aschamiddwoch und Karfreidog glegt.

So, und jetzt kemma zum Salvator, dem „Urvadda vom Doppebockbier“, wia da Zuber Martin, da Biersommelier und Diplom-Braumoasda vom Paulana Eiswerk, sogt. Des hod da Paulana-Mönch, da Still Valentin Stephan alias Bruda Barnabas, 1773 erfundn. Er war bis 1795 da Braumoasdda. Des war wos ganz Neis, wei des war bsondas süffig und brutal stark. „Des war quasi in da Fastazeit des Essn fia de Mönch, des war bsonders nahrhaft“, sogd da Zuber. „A untergärigs Doppelbockbier, bsondas moizig, leichdnd kup­fafarbn wega dem dunkln Gerstnmoiz.“

Da Salvator werd heid no fast so braut wia domois. „Er hod 18,3 Prozent Stammwürzn und 7,5 Prozent Alkohol“, sogd da Bierexperte. „Wega dem vuin Alkohol ­hoidd er se lang.“ Beim Paulana warns scho Hund und ham an Salvator schützn lassn. Awa de andan Brauarein san aa ned blääd und ham des „ator“ bei eahnare Biere (wie am Triumphator, Maximator) droghängt – jetz deaffas des aa Starkbier nenna. „A Doppebock muass awa mindestns 18 Prozent und da Bock 16 Prozent Stammwürzn ham“, woaß da Zuber. Da Nama Bock kimmt vo da Stodt Einbeck. Des is komisch, wei de liegt in Niedersachsen. „Awa de ham ois Ersdde an helln Bock hergstellt – und noch Bayern brocht“, sogd da Zuber.

Des Starkbier ghead jetz dazua – wiaras Dablecka vo de Politika. Näggsde Woch, am 24. Februar, do iss wieda so weit: Do werd de Bavaria, de Kinseher Luise, de Politika wieda gscheid eischenga – und de Leit an guadn Salvator dringa.

