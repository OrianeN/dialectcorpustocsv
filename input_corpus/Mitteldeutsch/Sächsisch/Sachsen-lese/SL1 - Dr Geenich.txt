Dr Geenich in Dule
Es war mal ä Geenich in Dule,

Där saß so bedriebt uffn Schtuhle

Un schtierte ganz dees‘ch vor sich hin.

Sei Härze, das bubberte bange,

Mr fiehlte: där macht‘s nich mähr lange.

(Wie das ähmd im Alder dud sin.)


So hockt‘r nu uff dr Därasse,

mit Gaffee schtand vor‘n änne Dasse,

Die liebt‘r mähr wie all sei Gäld.

Dänn‘s war ä Geschänk von sein Ännchen,

Die soff so bro Dag zwanzich Gännchen,

Drum mußt‘ se schon frieh aus dr Wäld.


Dr Geenich schrach: „Wenn ich ooch schtärbe,

Die Dasse gricht geener als Ärbe!“

Druff schmiß‘r se nunder ins Määr.

Dann lähnt‘r sich hin an de Bristung,

Un weil gar so schwär seine Ristung,

Da gollert‘r brombt hinterhär.