Gatzenjammer nachm Urloob
Draurich hocken alle wieder
Vor ihrn Bulte im Biro,
Uffn Schtuhl gebannt de Glieder,
Die noch gärzlich frei un froh.

Ach wie warsch vor värzen Daachen
Noch so härrlich an dr See,
Wo de Fälsenglibben raachen
Un de Meefen brilln "Juchheh !"

Oh, wie lieblich im Gebärche
Laatschte mer uff Gibfeln rum,
Unter sich Gerällgewärche
Un dr Mutschegihe Brumm'.

Ooch im wärzchen Nadelwalde
Warsch so iber alles scheen.
Ach, warum nur muß so balde
Wieder ins Geschäft mer gehn ?

Fuffzich lange Arweetswochen
Gehn bis nächsten Urloob hin...
Hättmer uns doch wo vergrochen,
Wo mer nich zu finden sin !