Dr Winter hot aah sei Freid
Dr Rauhreif glitzert, verweht sei de Wäller,

gefrurn dr Bach und dr Steg.

Wie Silber fünkeln und schimmern de Feller,

e Licht scheint ven Haus übern Weg.

Drin Uefen prasselt und knisterfs Reisig.

Do kumme de Hutzenleit.

E Blosballig knietscht, drin dr Steig singt dr Zeisig -

Dr Winter hoot aah sei Freid.

De Kinner fahrn Schiieten und Schnieschuh duebn Hübel,

mer drischt drin dr Schefs Getraa aus.

Zen Heilign Ummst laaft e Drehtorm drin Stübel,

dr Törk qualmt, und's Strueh raschelt daun Haus.

De Raacherkarzle duften, dr Engel schwebbt nieder

übern Kinnel‚ des drin dr Kripp leit,

und's liebe Bornkinnel kimmt alle Goahr wieder -

Dr Winter hoot aah sei Freid.