Maigäferlied
Mir sin vom Wonnemond dr Gluh.

Nu freilich, mir geheern drzu.

De Ginder hamm uns gärne.

Mir grabbeln in dn Birken rum

Un gondeln ahmds mit Mordsgebrumm

Um jede Waldladärne


Wenn uff dr Bank ä Bäärchen sitzt,

Gommt eener von uns angeflitzt

Un rutscht dr Maid in Gittel.

Dr Jinglink holt uns wieder raus,

Da ward schon manche Ehe draus-

Das is ä feines Mittel.


Doch drotzdäm mir so freindlich sin,

Da morden uns de Mänschen hin

Un nenn’ uns änne Blaache

Gäb’s uff dr Wält mähr Boesie,

Dann gäm ooch fier uns Gäfervieh

Ä bässres Los in Fraache.