Ick hebb t daan!
Dit is mien Bekenntnis: Ik hebb t daan! Eenfach so. Wat daar nakomen kunn för mi, dat was mi egaal. Ik hebb daar nich an docht. Wat weer wesen, wenn se mi snappt harren? Wat, wenn mi well tomööt komen weer? Wat, wenn ik van ´t Padd afkomen weer? In de een Ogenblick doch ik an nix anners, as an verleden Tieden. 
Wat harr dat jümmer för ´n Spaaß maakt. Gau dröver hen. Midden dör de Natur. Vögels und Blömen um mi herum. Af un an ok en Koppel mit Koorn. Koorn för Brood, Koorn för Beer. Grönet Land. De Duft van harde Arbeid in de Luft. Mennigmaal ok tovööl daarvan. Nöös hollen und wieder! Na de anner Sied hen. Daar, waar de Sünn achter d Höcht to Bedd geiht. Daar, waar sük en swartet Band an de Foot van de Höcht langslingt, mi de nächste Padd wiest. Wo gau dat gung! Een lüttje Ogenblick - un swupps weer ik daar. 
Dat hebbt se mi kött maakt. Dat dröfft nich mehr wesen. Daar harren se ´n Streek dör maakt. Verboden was ´t nu. Mit Straaf maakt se bang. Geld schall dat kösten, un de Naam word in en dicket Book noteert. Vör en ganz lang Tied. Kann ok wesen, dat word in so ´n neeimoodsken Komputer noteert. Punkt för Punkt. 
Egaal: Schiet is! Un ik mutt mi dat mit ansehn. Dag för Dag. Kom daar bilangs und dröff nich dör. Harr ik dat nich mit mien Geld betahlt? Nett as de annern ok. Ik hebb doch dat Recht daarto! Jede Dag weer jöökt mi dat in d´ Fingers. Wu weer t, see en Stimm in mi, wu weer t, wenn du so n beten wieder dreihst und denn mit ´n Swupp drupplangs? 
Man mien Öllern harren mi to en korrekten Minsk uptrucken. Jümmer an ´t Gesetz hollen! Nich af van ´t Padd komen! Lieken Weg gahn! Und denn leeg dat vör mi. Ik föhl mi so as Adam in ´t Paradies, de en söte Eva mit ´n Appel locken dee. Ne, wat kribbel mi dat in d´ Fingers und in d´ Foten. Ick kunn mi nich mehr hollen. Ik mook en gau Utschlag na rechts, un denn weer ik drup. Bössel daar langs. Achter mi de grode witte Klecks in d´ rode Kring. De harr mi nich mehr stoppen kunnt. Gau wieder. Ik luurde üm mi. Is daar een van de blau Wagens mit dat in de sülvig Farv tinkelnde Lücht drup to sehn? Nee! Glück hatt. 
Nu fangt dat an, mi Pläseer to maken. Ik, ganz alleen. Nüms anners achter mi un keneen vörut. För mi is de ganze Budel. Ik suus wieder. Vögels in Deckung, ik koom! 
Ji up de Raden - bisied! 
Ik suus as en Störmvogel daarhen. Vööls to froh was de Spaaß to Enn. 
Man, dat hett sük good anföhlt! Nüms, de mi uphollen hett. Keeneen, de mi snappt hett. Keen Straaf in Sicht. 
Knippke kann to blieven. 
Juchei! 
Anner Dag lees ik in d´ Anzeiger: Immer wieder fahren, trotz Fahrverbot, Autofahrer über die gesperrte Bensersieler Umgehungsstraße. 
Ik bün een daarvan west. 
Dat geev ik to. 
Anneus Buisman, Esens 11.11.2017 