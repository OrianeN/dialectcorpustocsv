Die Weihnachtsgeschichte auf Berlinerisch

Zu jenau die Zeit hat Kaisa Aujustus 'n Jesetz jemacht, wat besachte,
det nu alle Leute Steuern zahln solln. Det war det erstemal un
passierte, wie Cyrenius in Syrien Chef vons janze war. Wejem dem
jingen alle in die Stadt, wo se jeborn warn. So hat det ooch Joseph
aus Galiläa jemacht, der jing von Nazareth Richtung Bethlehem in
Judäa, wo David seine Familie herstammte. Da sollta sich melden mit
Maria, seine Braut, die jing schwanga. Und wie se da anjekomm'n sind,
war't soweit, det se dit Jör kriejen sollte. Und so bekam se denn 'n
Sohn, wickelte ihn und packte ihn inne Krippe, weil wo anders keen
Platz nich war.

Janz inne Nähe warn Hirtn, die inne Nacht uff'm Feld vor de Schafe
uffjepaßt habm. Und kick ma, da taucht plötzlich Jottes Engel uff, un
justemang war't tachhell, un da krichten se't mit de Angst. Un der
Engel sachte: "Nu man keene Bange nich! Wat ick zu sajen hab', läßt
bei euch un alle andern jroße Freude uffkomm'n. Det is nämlich so: Da
is heute in olle David seine Stadt eener jeborn wor'n, den nenn' se
Christus un det is'n janz besondera. Den erkennta daran, det er
jewickelt inne Krippe liecht." Und denn kam noch'n janza Hauf'n
Engels, die ham alle zusamm'n jesung'n: "Ehre sei Jott inne Höhe un
Friede uff Erden un 'n Menschn een Wohljefalln!"

Wie die Engels dann wieda wechjeflojen sind, sachte een Hirte zu de
andern: Komm, laßt uns ma kicken jehn, ob det ooch wah is, wat die
uns da erzählt ham. Un da kam'n se ooch schon eilich jeloofen un
trafen ooch uff Maria un Joseph mit ihr'm Kleenen, wat unjelogen inne
Krippe lach. Un wie se't jesehn hattn, da ha'm se det weitajesacht,
wat se von det Kind jehört ha'm. Un die Leute, den se't jesacht ha'm,
ha'm sich jewundat über det, wat se vonne Hirten jehört ha'm. Un
Maria wa mächtich jerührt und hat ville darüba jejrübelt. Zu juta
Letzt sind de Hirten wieda nach Hause jejan'n, haben 'n lieben Jott
jepriesen un jelobt für allet, wat se jehört und jeseh'n ha'm, so wie
et ihnen jesacht wor'n war.