Kreuzberch: Dit Schlesische Tor
Dit SCHLESISCHES TOR im nordöstlichen Teil von Kreuzberch jehörte ursprünglich zur den Stadttoren Berlins. Von hier aus jing et in Richtung Schlesien, womit ooch jleich der Name erklärt wäre. Die Stadtmauer verschwand beireits im 19. Jahrhundert und mit ihr ooch dit „Schlesische Stadttor“.
 Jeblieben iss der Name, ansonsten hat sich hier ne typisch innastädtische Jejend entwickelt. Dominierend iss hier der Hochbahnhof mit jleichem Namen. Der wurde zwischen 1899-1901 nach Plänen der Architekten Hans Grisebach (1848-1904) und August Dinklage (1849-1920) im historistischen Stil jebaut. Der Station jing zusammen mit der Hochbahnlinie am 15. Februar 1902 in Betrieb. Der Bahnhof wurde mehrfach aweitert und übadauerte den 2. Weltkrieg mit eha leichten Schäden und steht heute unta Denkmalschutz.
 Einschneidend für die Jejend war der Bau der Mauer 1961. Damit wurde sie zur Randlaje und die Hochbahn Richtung Warschauer Straße endete hier. Nach dem Mauerfall 1989 belebte sich dit Schlesische Tor aneut und ooch der Bahnhof wurde wieda zur Durchjangsstadion.
 Amtlich jesehn jibt et dit SCHLESISCHE TOR eijentlich jarnich. Et handelt sich vielmehr um die Kreuzung der Oberbaumstraße, Schlesische Straße, Skalitzer Straße und Köpenicker Straße. Die Kreuzung ahielt nie nen offiziellen Namen, wird aba nach wie vor in Stadtplänen wie im Volksmund als SCHLESISCHES TOR bezeichnet.
 Umjeben iss die Kreuzung und der Bahnhof von villen ahaltenen Altbauten aus der Jründazeit. Bemerkenswert iss die ehemalije öffentliche Bedürfnisanstalt unta dem Hochbahnviadukt. Hierbei handelt et sich um eene auf Bauteilen des Café Achteck arrichtete WC-Anlaje, alladings in viereckija Form. Als WC wird dit Häuschen aba schon lange nich mehr jenutzt und dient heute als Imbissbude.
Dit Schlesische Tor – sichalich keene jroße Sehenswürdichkeit und dennoch interessant.
Aussteijen lohnt sich…!
 
 
 
