Rotwelsch im Berliner Dialekt: Kohldampf, Polente, Schmu und Co.
Der Berliner Dialekt, streng jenommen iss er ja een Metrolekt, war sehr villen sprachlichen Einflüsse in der Jeschichte unterworfen. in eenem früheren Artikel uff diesen Blog jing et ja schon um den Einfluss der französischen Sprache uff dit Berlinerisch.
 Wenija bekannt iss die Sprache um die et diesmal jeht, nämlich Rotwelsch. Bei Rotwelsch handelt et sich um Sprachen soziala Randjruppen die seit dem späten Mittelalta z.B vom sojenannten fahrenden Volk wie z.B Hausierer oda reisende Musikanten, Bettlern, Mitjliedern „unehrlicha Berufe“, bis hin zu Kriminellen jesprochen wurden. Manchmal wird die Sprache deshalb ooch als Jaunasprache bezeichnet. Macht man heute aba eha nich mehr, um nich alle Bevölkerungsjruppen zu stigmatisieren die Rotwelsch jesprochen haben und trotz soziala Not vasuchten ihren Lebensuntahalt durch Arbeet zu vadienen.
 Man findet jede Menge Sprachen im Rotwelsch wieda, so z.B. Romani, Westjiddisch, Französisch, Italienisch oda Niederländisch. Dit liecht daran, dess sich die Mitjlieder der jenannten Bevölkerungsjruppen aus den untaschiedlichsten Rejionen und Staaten zusammensetzten und natürlich lechte dit fahrende Volk jroße Entfernungen zurück und kam durch die untaschiedlichsten Länder.
 Schließlich wurden jroße Teile der Rotwelschsprecher seßhaft, so nach dem dreißigjährijen Krieg.  Damit floß een Teil vom Wortschatz in einijen rejionalen Dialekten und der alljemeenen Umjangssprache ein.
Dazu jehört ooch Berlinerisch. Hier habe ick mal eene kleene Auswahl von Wörtern aus dem Rotwelsch erstellt, die in den Berliner Dialekt einjeflossen sind:
Bammel – Angst
 blechen – bezahlen
 Kaschemme – schlechte, miese Gastwirtschaft
 Klamotten – Kleidung
 Kohldampf – starker Hunger
 Moneten – Geld
 mopsen – stehlen
 pennen – schlafen
 Polente – Polizei
 Schmu – Betrug
 Zasta – Geld

 
 
