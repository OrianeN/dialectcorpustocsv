Berliner Küche: Kartoffelpuffa
Kartoffelpuffa mit Appelmus und Zucka jehören zu den typischen Jerichten der Berliner Küche. Sie sind preiswert und eenfach in der Zubereitung. Selbst jemacht sind se alladings nen bisken zeituffwendich. Kartoffeln schälen und danach zusammen mit Zwiebeln reiben jeht nu ma nich in 5 Minuten. Der Zwiebeln wejen würd wohl ooch so manche Träne fließen. Dafür weeß man wat ma jetan hat und et schmeckt natürlich bessa als aus dem Tiefkühlfach.
Die kleenen knusprijen Kartoffelfladen findet man ooch in den Küchen anderer deutschsprachijer Regionen und so jibt et ne Reihe von Namen für den Puffa. So wird er in West- und Norddeutschland als Reibekuchen bezeichnet. Im Rheinland (und uff Kölsch) heeßt er Rievkooche. In Hessen kennt man ihn unta der Bezeichnung Erbelkrebbel. In Bayern wird denn daraus der Erdäpfelpuffer oder Reiberdatschi. Ooch in Österreich sacht man Erdäpfelpuffer. Zudem sind se hier als Erdäpfelkrapferl bekannt. Die Eidjenossen nennen se Härdöpfeldätschli oda Härdöpfelchüechli und die Luxemburger als Gromperekichelcher.

Nich deutschsprachije Namen für den Puffa sind z.B. Bramborák (Tschechien), Platzki (Polen), Draniki (Russland) oda Deruny (Ukraine).

Dit iss nur ne kleene Namensauswahl. Jerne können hier weitere Namen als Kommentar hintalassen werden.
Natürlich jibt et bei den Beilajen ebenfalls zahlreiche Varianten. Man kann se mit Quark savieren, nen Julasch oda ooch Lachs dazu essen. Hier kennt der menschliche Jeschmack wohl keene Jrenzen und der Lesa iss jern einjeladen dazu nen Kommentar zu hintalassen.
Nen orijinal Berliner Rezept für Puffa jibt et nu hier:
Zutaten (4 Personen):

1,5 kg Kartoffeln (bevorzugt mehlig kochend, vorwiegend festkochend jeht aba ooch)
3 Zwiebeln
3 Eia
2 Esslöffel Mehl
1 Prise Salz
1 Prise Zucka
Pflanzenöl zum braten
Zubereitung:
Die Kartoffeln zunächst schälen und waschen. Denn mit eena Küchenreibe inne Schüssel reiben. Mit den Zwiebeln machen wa dit selbe und vamischen se mit der Kartoffelmasse. Hinzu kommen die Eia, dit Mehl und je nach Jeschmack dit Salz und der Zucka. Wir rühren die Masse noch ma ordentlich durch.
In der Pfanne ahitzen wa Pflanzenöl und jeben mit nen Küchenlöffel jeformte runde Kartoffelfladen in die Pfanne. Diese sollten nich zu dünne sein, sonst vabrutzeln se nämlich. So braten wir die Puffa ca. 5-10 Minuten von beeden Seiten joldbraun. Schließlich lässt man se uff Küchenpapier oda nen Sieb kurz abtropfen und saviert se mit den ausjewählten Beilajen seinen Jästen oda ooch nur sich selba.
Na denn, JUTEN APPETIT!
 
 

 
 
 
 
