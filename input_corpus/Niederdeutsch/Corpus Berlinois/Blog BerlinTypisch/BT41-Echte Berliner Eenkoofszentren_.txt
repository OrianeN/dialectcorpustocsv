Echte Berliner Eenkoofszentren!
Berlin iss ja inzwischen zu jepflastert mit Eenkoofszentren. Eens sieht wie dit andere aus und man bekommt dort dit übliche jlobalisierte Warensortiment. Eben dit, wat et ooch in New York, London oda Paris jibt. Wie ville et davon jibt, hab ick nich jezählt. Ick weeß aba, des et noch vier echte Berliner Eenkoffszentren jibt, jenau jesacht Markthallen. Ursprünglich waren et 14 sojenannte historische Hallen, die ab 1883 errichtet wurden. Von denen haben jerade mal drei in der klassischen Form der Markthalle überlebt:
– Arminiusmarkthalle (ehemalije Markthalle X in Moabit, ooch Moabiter Markthalle jenannt,
  wird heute offiziell als Zunft[halle] Arminiusmarkthalle bezeichnet.
Ort: Arminiusstraße 2-4, 10551 Berlin

– Eisenbahnhalle (ehemalije Markthalle IX in Kreuzberg)
Ort: Eisenbahnstraße 42/43, 10997 Berlin
– Marheineke-Halle (ehemalije Markthalle XI, ebenfalls in Kreuzberg)
Ort: Marheinekeplatz/Bergmannstraße, 10961 Berlin

Alle anderen ursprünglichen Hallen stehen entweder nicht mehr oda werden anderweitich jenutzt. Natürlich sind noch späta jede Menge weitere errichtet und wieder abjerissen oda umjenutzt worden. Aba ooch von diesen Hallen iss nur noch übrich jeblieben die:
– Markthalle Berlin-Tegel
Ort: Gorkistraße 15, 13507 Berlin
Wär also Mal nen bissken „Berliner Flair“ erleben will, sollte mal in die jenannten Hallen vorbei kieken. Natürlich jibt et hier inzwischen ooch jede Menge internationalet. Vielet kommt jedoch aus der Region. Backwaren, noch richtije Fleisch- und Wurschtwarenstände, Fisch,  Obst und Gemüse und jede Menge Essensanjebote, darunta natürlich die Berliner Küche. Na und denne hört man hier dit Berlinerisch, hier iss die Berliner Schnauze imma noch zu Hause. Aba nich aschrecken, se wissen ja, wie et uff Märkten so zujeht…
Uff diesen Blog werde ick die vier verbliebenen Berliner Markthallen zukünftich der Reihe nach jenau vorstellen. Kieken se also imma ma wieda vorbei und lassen se sich ent- und vaführen!

