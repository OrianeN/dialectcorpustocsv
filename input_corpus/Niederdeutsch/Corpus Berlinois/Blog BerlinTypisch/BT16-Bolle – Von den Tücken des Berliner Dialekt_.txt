Bolle – Von den Tücken des Berliner Dialekt!

Der olle Bolle und die Bolle, tückisch, wat een Dialekt Unkundijer da so allet mit aleben kann.
 Nehmen wa Tourist Gunter M., Herkunftsort unbekannt, fuhr er zum ersten Mal nach Berlin. Oberflächlich hatta sich ja mit dem Berliner Jargon vatraut jemacht. Aba die Tücke steckt eben im Detail. Nu wusste er, des ne Bolle in Berlin nen Loch im Strumpf iss. Da plagte ihm der Hunga, weshalb er nen orijinal Berliner Restaurant uff suchte. Doch wat las er da? Bollenfleesch jabs hier heute. „Fürchterlich“ sachte er zum Kellner, „Fleisch in einer löchrigen Socke?“ Doch der klärte Gunter M. uff: „Ne, ne Meesta, dit iss in Berlin Lammfleesch mit ville Zwiebeln, Bolle iss nämlich in Berlin ooch ne Zwiebel. Na Meesta, wie wär et damit?“ So bestellte Gunter M. sich also Bollenfleesch, mit dem er sich seene Wampe vollschlug. Satt und zufrieden verließ er schließlich dit Restaurant, um sich die Stadt weita anzukieken. Weila so zufrieden war und grinte, frachte ihn een Berliner: „Mensch, wat denn so lustich, freuste da ja wie Bolle!“ Da aschrak Gunter M. und sachte zu sich: “Man, wie schau ich denn gerade aus, wie ne Zwiebel oder nen Loch im Strumpf?“ Nee, nee Gunter M., dit war nich ironisch jemeint. Der Mann meente dit ernst mit dem freuen. Hätteste ma dit alte Berlina Volkslied vom ollen Bolle jekannt, der sich imma freute und von dem die Redewendung stammt.
 Mit dem Milchkoufmann Carl Bolle (Bimmel-Bolle) hatte der übrijens nüscht zu tun. Der betrieb nämlich früha in Berlin ne Meierei, aus der späta nen Supamarkt hervor jing. Den Supamarkt gibt et nich mehr, aba dennoch sajen ville Leute imma noch, wenn se inkoffn jehen wollen: „Ick muss ma eben noch zu Bolle.“ Ooch die Redewendung „Preise wie bei Bolle“ jeht uff  Carl Bolle zurück.
 Ebenso jibt et ne „Bollefete“. Damit iss denn ne Fete jemeint, wo jeda irjendwat zu futtern oda zu trinken mitbringt.
Ne kesse Bolle hinjejen iss ne kleene Jöre, die imma nen vorlauten Spruch uff de Lippen hat.
 Nu reichts aba mit Bolle. Oda fällt jemanden noch wat zum Thema Bolle in?
Den Text vom Bolle Lied jibs übrijens hier:


  
