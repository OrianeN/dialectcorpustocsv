Backwaren uff Berlinerisch: Brötchen
Brötchen jibt et heutzutage jede Menge. Helle, dunkle, runde, viereckije, dreieckije und wat sich die Marketingabteilungen der jlobalisierten Backshops noch so allet ausjedacht haben. Benannt mit wohlklingenden Namen liejen se tonnenweese in den Selbstbedienungsboxen der Filialketten.
 Manch een Brötchen schmeckt ooch janz jut, doch dit Herz von nem echten Berliner lassen die vier Klassika der Berliner Backkunst höha schlajen. Die Rede iss von der SCHRIPPE, dem KNÜPPEL, den SCHUSTAJUNGEN und dem SPLITTABRÖTCHEN. Dabei iss die Schrippe als weißet Weizenbrötchen wohl am weitesten vabreitet. Se füllt die Rejale unzählija Bäckereien und Backshop der Stadt.
Natürlich kann man se ooch selba machen. Darum habe ick hier mal nen eenfachet Rezept vorjekramt. Eenfach mal ausprobieren, iss eenfacha jemacht als jedacht!

Zutaten für 10 Schrippen:
350g Weizenmehl (Typ 550)
20g Hefe
10g Marjarine
200ml kaltet Wassa
1 Prise Salz
1/2 Prise Zucka
Zubereitung:
Dit Mehl sieben. Die in Wassa uffjelöste Hefe und die übrijen Zutaten mischen und eenen nich zu festen Teig kneten. Denn den Teig ca. 45 Minuten lang abjedeckt ruhen lassen, damit er „jehen“ kann. Nach der Ruhezeit teilt man den Teig in 10 Teile und formt aus jedem Teil längliche Rohlinge. Die schneidet man denn an der Obaseite mit nem scharfen Messa ein und lecht se uff een einjefettetet Backblech und bäckt die Brötchen bei 220 Jrad ca. 20-25 Minuten joldbraun. Schließlich lässt man die Schrippen abkühlen und saviert se zum Frühstück!
JUTEN APPETIT!

