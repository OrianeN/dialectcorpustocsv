dialect_families_abbr = {'Mitteldeutsch': 'Mitteldeutsch',
                         'Niederdeutsch': 'Niederdeutsch',
                         'Oberdeutsch': 'Oberdeutsch'}

dialects_abbr = {'Kölsch': 'Kölsch',
                 'Sächsisch': 'Sächsisch',
                 'Corpus Berlinois': 'Berlinisch',
                 'Corpus Plattdeutsch - plattdüütsch': 'Plattdeutsch',
                 'Bairisch': 'Bairisch',
                 'Alemannisch': 'Alemannisch',
                 }
